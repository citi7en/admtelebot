#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import telebot
import os
import pathlib
import sys
import argparse
import subprocess


def bot_help():
    with open('help.txt', 'r') as f:
        help_string = f.read()

    return help_string


def get_sys_info():
    pass


def open_process(command=[], status=True):
    if status:
        process_text = subprocess.getstatusoutput(command)
        return process_text[0]
    else:
        process_text = subprocess.run(command)
        return process_text
    pass


def start_bot():
    if len(sys.argv) > 1:
        try:
            file = open(sys.argv[1], 'r')
            TOKEN = file.read().strip()
            file.close()

        except FileNotFoundError:
            print("%s not found!\nUsage: bot.py [file_with_token]" % sys.argv[1])

    else: exit(0)

    bot = telebot.TeleBot(TOKEN)

    @bot.message_handler(content_types=['text'])
    def text_messages(message):
        human_ask = message.text
        human_id = message.from_user.id
        human_name = message.from_user.first_name
        bot_responds = bot.send_message

        if human_ask == "test":
            bot_responds(human_id, 'test test test')
            # bot.send_message(message.from_user.id, "тестовое сообщение")
            print("Пользователь [%s] запросил [%s]" % (human_name, human_ask))

        elif human_ask == "ping":
            bot_responds(human_id, "Проверка доступности сайта www.admrad.ru (beta)\n")
            
            print("Пользователь [%s] запросил [%s]" % (human_name, human_ask))

        elif human_ask == "time":
            time = subprocess.run('date', shell=True, check=True, stdout=subprocess.PIPE, universal_newlines=True)
            bot_responds(human_id, time.stdout.strip())
            print("Пользователь [%s] запросил [%s]" % (human_name, human_ask))

        elif human_ask == "ip":
            ip = subprocess.run('ip a | grep 127', shell=True, check=True, stdout=subprocess.PIPE, universal_newlines=True)
            bot_responds(human_id, ip.stdout.strip())
            print("Пользователь [%s] запросил [%s]" % (human_name, human_ask))

        else:
            bot_responds(message.from_user.id, bot_help())
            print("Пользователь [%s] запросил [%s]" % (human_name, human_ask))

    bot.polling()


if __name__ == "__main__":
    try:
        start_bot()

    except KeyboardInterrupt:
        pass
